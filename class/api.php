<?php

class Api{
	private $result;
    private $msgErro;
    
    public function Api(){}

	public function __construct(){}

	public function getResult(){
        return $this->result;
    }
    public function setResult($result){
        $this->result = $result;
    }

    public function getMsgErro(){
        return $this->msgErro;
    }
    public function setMsgErro($msgErro){
        $this->msgErro = $msgErro;
    }
}