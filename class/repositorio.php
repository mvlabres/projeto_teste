<?php

class Repositorio
{
    private $id;
	private $name;
    private $avatar;
    private $star;
    private $fork;
    private $data;

    public function __construct(){}

    public function Repositorio(){}
      
    public function getId(){
        return $this->id;
    }
    public function setId($id){
        $this->id = $id;
    }

    public function getName(){
            return $this->name;
    }
    public function setName($name){
        $this->name = $name;
    }

    public function getAvatar(){
        return $this->avatar;
    }
    public function setAvatar($avatar){
        $this->avatar = $avatar;
    }

    public function getStar(){
        return $this->star;
    }
    public function setStar($star){
        $this->star = $star;
    }

    public function getFork(){
        return $this->fork;
    }
    public function setFork($fork){
        $this->fork = $fork;
    }

    public function getData(){
        return $this->data;
    }
    public function setData($data){
        $this->data = $data;
    }
}
?>