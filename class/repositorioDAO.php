<?php
    class RepositorioDAO
    {
        public function __construct(){}
    
        public function RepositorioDAO(){}

        public function insert($MySQLi, $query){
            try{
                if($MySQLi->query($query)){
                    return true; 
                }else return false;
            }catch(Exception $e){
                return false;
            } 
        }
        public function update($MySQLi, $query){
            try{
                if($MySQLi->query($query)){
                    return true; 
                }else return false;
            }catch(Exception $e){
                return false;
            } 
        }
        public function select_count($MySQLi){
            try{
                $sql = 'SELECT * FROM repositorios';
                $result = $MySQLi->query($sql);
                return $result->num_rows; 
            }catch(Exception $e){
                return false;
            } 
        }
        public function select_nameId($MySQLi){
            try{
                $sql = 'SELECT id, name FROM repositorios';
                return $MySQLi->query($sql); 
            }catch(Exception $e){
                return false;
            } 
        }
    }
?>