<?php

function getRequest($api, $post)
{
    require_once '../class/repositorio.php';
    require_once 'repositorio-controller.php';

	$cInit = curl_init();
	curl_setopt($cInit, CURLOPT_URL, $post['url']);
	curl_setopt($cInit, CURLOPT_RETURNTRANSFER, 1); 
	curl_setopt($cInit, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
	curl_setopt($cInit, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	curl_setopt($cInit, CURLOPT_USERPWD, $post['username'] . ':' . $post['password']);

    $api->setResult(curl_exec($cInit));
    
    curl_close($cInit);

    $api->setResult(json_decode($api->getResult()));
    $result = $api->getResult();
    if(!property_exists($result, 'message')){
        $arrayRepositorio = array();
        foreach($result->items as $consumidor){
            $repositorio = new Repositorio();
            $repositorio->setAvatar($consumidor->owner->avatar_url);
            $repositorio->setName($consumidor->name);
            $repositorio->setStar($consumidor->stargazers_count);
            $repositorio->setFork($consumidor->forks_count);
            array_push($arrayRepositorio, $repositorio);
        }
        if(addBD($arrayRepositorio) == true){
            return $arrayRepositorio;
        }else return false;
    }else{
        return false;
    }
}
    
?>