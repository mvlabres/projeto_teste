<?php

    function addBD($arrayRepositorios){

        require_once '../class/repositorioDAO.php';
        require_once '../conn.php';

        $repositorioDAO = new RepositorioDAO();

        date_default_timezone_set("America/Sao_Paulo");
        $date = date('Y-m-d');
        $query = '';
        if($repositorioDAO->select_count($MySQLi) == 0 ){
            foreach ($arrayRepositorios as $key => $value){
                $sql = "insert into repositorios (name, stars, forks, url_avatar, data) ";
                $sql .= "values( ";
                $sql .= "'".$value->getName()."', ";
                $sql .= $value->getStar().", ";
                $sql .= $value->getFork().", ";
                $sql .= "'".$value->getAvatar()."', ";
                $sql .= "'".$date."');";
                $repositorioDAO->insert($MySQLi, $sql);
            }
            return true;
        }else{
            $repositorios = $repositorioDAO->select_nameId($MySQLi);

            $array_repositorio_bd = array();
            while ($dados = $repositorios->fetch_assoc()){
                array_push($array_repositorio_bd, $dados['name']);
            }
            
            foreach ($arrayRepositorios as $key => $value){
                if (in_array($value->getName(), $array_repositorio_bd)){
                    $sql = "update repositorios set ";
                    $sql .= "name= '".$value->getName()."', ";
                    $sql .= "stars= ".$value->getStar().", ";
                    $sql .= "forks= ".$value->getFork().", ";
                    $sql .= "url_avatar= '".$value->getAvatar()."', ";
                    $sql .= "data= '".$date."' ";
                    $sql .= "where name = '".$value->getName()."';";
                    $repositorioDAO->update($MySQLi, $sql);
                }else{
                    $sql = "insert into repositorios (name, stars, forks, url_avatar, data) ";
                    $sql .= "values( ";
                    $sql .= "'".$value->getName()."', ";
                    $sql .= $value->getStar().", ";
                    $sql .= $value->getFork().", ";
                    $sql .= "'".$value->getAvatar()."', ";
                    $sql .= "'".$date."');";
                    $repositorioDAO->insert($MySQLi, $sql);
                }
            }
            return true;
        }
    }
?>