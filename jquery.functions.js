$(function(){
    $("#input-search").keyup(function(){

    var table = $(this).attr('alt');
        if( $(this).val() != ""){
            $("."+table+" tbody>tr").hide();
            $("."+table+" td:contains-ci('" + $(this).val() + "')").parent("tr").show();
        } else{
            $("."+table+" tbody>tr").show();
        }
    }); 
});
$.extend($.expr[":"], {
    "contains-ci": function(elem, i, match, array) {
        return (elem.textContent || elem.innerText || $(elem).text() || "").toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
    }
});



$(document).ready(function () {
    $('#lang').click(function (){
        $(document).translate($(this).attr('name'));
    });
    
    $(document).translate($('#lang').val());
});

(function($) {

var locales = {
    'pt-BR': {
        'msg' : {message: 'Informe seu nome de usuário e senha do GitHub'},
        'btn' : {message: 'Buscar'},
        'name' : {message: 'Nome'},
        'star' : {message: 'Estrelas'},
        'fork' : {message: 'Garfos'},
        'eng' : {message: 'Inglês'},
        'pt' : {message: 'Português'},
        'user' : {message: 'Usuário'},
        'pass' : {message: 'Senha'}
    },
    'en': {
        'msg' : {message: 'Enter your GitHub username and password'},
        'btn' : {message: 'Search'},
        'name' : {message: 'Name'},
        'star' : {message: 'Stars'},
        'fork' : {message: 'Forks'},
        'eng' : {message: 'English'},
        'pt' : {message: 'Portuguese'},
        'user' : {message: 'Username'},
        'pass' : {message: 'Password'}
    }
};

function translate(id, locale) {
    var l = locales[locale];

    if (!l) {
        l = locale.en
    }
    return l[id];
}

$.fn.translate = function(locale) {
    var elements = this.find('[data-translation-id]');

    $.each(elements, function () {
        var id = $(this).data('translation-id');
            t = translate(id, locale);
        if (t.isBodyHTML) {
            $(this).html(t.message);
            $('#img-change').attr("src", "../img/united_kingdom.ico");
            $('#img-dropDown').attr("src", "../img/brazil.ico");
        } else {
            $(this).text(t.message);
        }
    });
    if($('#img-change').attr("src") === "../img/united_kingdom.ico"){
        $('#img-change').attr("src", "../img/brazil.ico");
        $('#lang').text("Portuguese");
        $('#img-dropDown').attr("src", "../img/united_kingdom.ico");
        $('#lang').attr("name", "pt-BR");
        $('#input-search').attr("placeholder", "Search");
    }else{
        $('#img-change').attr("src", "../img/united_kingdom.ico");
        $('#lang').text("English");
        $('#img-dropDown').attr("src", "../img/brazil.ico");
        $('#lang').attr("name", "en");
        $('#input-search').attr("placeholder", "Pesquisar");
    }
};

}(jQuery));



$(function(){
    $(".btn-toggle").click(function(e){
        e.preventDefault();
        el = $(this).data('element');
        $(el).toggle();
    });
});

$('.alert').alert()