<?php
    function notification_error($parameter){
        //switch para implementar outras solicitações de notificação
        switch($parameter){
            case 'credentials': {
                $msg = "Verifique suas credenciais ou sua conexão com a internet e tente novamente.";
                break;
            }
        }
        echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">
        '.$msg.'
        <button type="button" class="close btn-danger" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>';
    }

?>