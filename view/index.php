<?php 
include_once('../class/api.php');
include_once('../controller/api-controller.php'); 
include_once('../notification.php');

ini_set( 'display_errors', true );
error_reporting( E_ALL );

$api = new Api();

$arrayRepositorio = null;
if(isset($_POST['username']) && $_POST['username'] != null){
    $arrayRepositorio = getRequest($api, $_POST);
    if(!$arrayRepositorio){
        notification_error('credentials');
    }
}
if($arrayRepositorio == null) $visible = false;
else $visible = true;
$url = 'https://api.github.com/search/repositories?q=language:Java&sort=stars&page=1';


?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>API GIT HUB</title>
        <link rel="shortcut icon" href="../img/git.ico">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" href="../style.css">
        <script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
        <script type="text/javascript" src="../jquery.functions.js"></script>
    </head>
    <body>
        <div >
            <nav class="nav-git navbar navbar-dark bg-dark">
                <div class="nav-response">
                    <img class="img-nav" src="../img/logo.png">
                    <small class="text-nav text-muted">&nbsp Api GitHub</small>
                </div>
                <form class="div-search form-inline ">
                    <div class="div-search-items div-group-git input-group">
                        <input alt="table" type="text" id="input-search" class="form-control" aria-describedby="basic-addon1" placeholder="Pesquisar">
                    </div>
                </form>
                <div class="div-drop dropdown">
                    <button class="btn btn-link" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        <img id="img-dropDown" style="width: 30px;" id="lang" name="pt-BR" class="icon-lang" src="../img/brazil.ico">
                        <span class="caret"></span>
                    </button>
                    <ul class="div-dropdown list-group-item-dark dropdown-menu" aria-labelledby="dropdownMenu1">
                        <li><img id="img-change" class="icon-lang" src="../img/united_kingdom.ico"><a href="#" id="lang" name="en" >Inglês</a></li>
                    </ul>
                </div>
            </nav>
            <div class="div-main main-panel">  
                <div class="card">
                    <div class="card">
                        <div class="panel-teste panel-body">
                            <div class="card-body">
                                <h6 class="card-title" data-translation-id="msg">Informe seu nome de usuário e senha do GitHub</h6>
                                <div class="main-panel"> 
                                    <form method="post" action="#">
                                        <div class="row-git">
                                            <div class="col">
                                                <label data-translation-id="user">Usuário</label>
                                                <input name="username" type="text" class="form-control" required>
                                            </div>
                                            <div class="col">
                                                <label data-translation-id="pass">Senha</label>
                                                <input name="password" type="password" class="form-control" required>
                                            </div>
                                            <div class="col btn-start-end">
                                                <button type="submit" class="btn btn-primary" data-translation-id="btn">Buscar</button>
                                            </div>
                                            <input type="hidden" name="url" value="https://api.github.com/search/repositories?q=language:Java&sort=stars&page=1'">
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div data-element="#minhaDiv">
                                <table class="table" visible="<?=$visible ?>">
                                    <thead id="lang" > 
                                        <tr>
                                            <th>Avatar</th>
                                            <th data-translation-id="name">Nome</th>
                                            <th data-translation-id="star">Estrelas</th>
                                            <th data-translation-id="fork">Garfos</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                            <?php
                                                if($arrayRepositorio != null){
                                                    foreach ($arrayRepositorio as $key => $value){
                                                        echo "<tr id='tr'><td><img class='circle' src='".$value->getAvatar()."'></td>";
                                                        echo "<td>".$value->getName()."</td>";
                                                        echo "<td>".$value->getStar()."</td>";
                                                        echo "<td>".$value->getFork()."</td>";
                                                        echo "</tr>";
                                                    }
                                                }    
                                            ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
